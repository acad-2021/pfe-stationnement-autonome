clear
close all


%Cercle en haut de la courbe
R_cercle_haut = 2;
X_C_cercle_haut = 5;

%Cercle en bas de la courbe
R_cercle_bas = 1;
X_C_cercle_bas = 2;


colors = {'b','r','g'};

%% Definition des points de depart et d'arrivee
%Point initiale de la voiture
Pt_init(1,1) = 10;
Pt_init(1,2) = 5;

%Point arrive de la voiture
Pt_arrive(1,1) = 0;
Pt_arrive(1,2) = 0;

%% Definition des 2 cercles principaux
% Intervalle entre 2 points du cercle
th = 0:pi/100:2*pi;

%Cercle en haut de la courbe
%R_cercle_haut = 2;
C_cercle_haut = [X_C_cercle_haut Pt_init(1,2)-R_cercle_haut];

Coor_cercle_haut =  zeros(size(th,2),2);
Coor_cercle_haut(:,1) = R_cercle_haut * cos(th) + C_cercle_haut(:,1);
Coor_cercle_haut(:,2) = R_cercle_haut * sin(th) + C_cercle_haut(:,2);

%Cercle en bas de la courbe
%R_cercle_bas = 1;
C_cercle_bas = [X_C_cercle_bas Pt_arrive(1,2)+R_cercle_bas];

Coor_cercle_bas =  zeros(size(th,2),2);
Coor_cercle_bas(:,1) = R_cercle_bas * cos(th) + C_cercle_bas(:,1);
Coor_cercle_bas(:,2) = R_cercle_bas * sin(th) + C_cercle_bas(:,2);

%% Definition des 2 points tangents aux droites
%Point le plus haut de la courbe
Pt_haut(1,1) = C_cercle_haut(:,1);
Pt_haut(1,2) = Pt_init(1,2);

%Point le plus bas de la courbe
Pt_bas(1,1) = C_cercle_bas(:,1);
Pt_bas(1,2) = Pt_arrive(1,2);

%% Calcul des tangentes interieures
%Site de reference https://lucidar.me/fr/mathematics/tangent-line-segments-to-circle/
% Circle A
xA=C_cercle_bas(:,1);
yA=C_cercle_bas(:,2);
RA=R_cercle_bas;

% Circle B
xB=C_cercle_haut(:,1);
yB=C_cercle_haut(:,2);
RB=R_cercle_haut;

% Draw circles
% a=[0:pi/16:2*pi];
% Xa=xA+RA*cos(a);
% Ya=yA+RA*sin(a);
% plot (Xa,Ya,'k');
% hold on;
% axis square equal;
% grid on;
% plot (xA,yA,'xk');
% 
% 
% Xb=xB+RB*cos(a);
% Yb=yB+RB*sin(a);
% plot (Xb,Yb,'k');
% plot (xB,yB,'xk');

% Compute distance between circle centers
D=sqrt ( (xB-xA)^2 + (yB-yA)^2 );

coeff_direct_tangente = zeros(2,1);

if (D^2 - (RA+RB)^2>=0)
    
    % Compute the length of the tangents
    L=sqrt(D^2 - (RA+RB)^2);

    % Compute the parameters
    R1=sqrt(L^2+RB^2);    
    Sigma1= (1/4) * sqrt ( ( D+RA+R1 )*( D+RA-R1 )*( D-RA+R1 )*( -D+RA+R1 )  );
    R2=sqrt(L^2+RA^2);
    Sigma2= (1/4) * sqrt ( ( D+RB+R2 )*( D+RB-R2 )*( D-RB+R2 )*( -D+RB+R2 )  );
    
    
    % Compute the first tangent
    x11= (xA+xB)/2 + (xB-xA)*(RA^2-R1^2)/(2*D^2) + 2*(yA-yB)*Sigma1/(D^2);
    y11= (yA+yB)/2 + (yB-yA)*(RA^2-R1^2)/(2*D^2) - 2*(xA-xB)*Sigma1/(D^2);
    x21= (xB+xA)/2 + (xA-xB)*(RB^2-R2^2)/(2*D^2) + 2*(yB-yA)*Sigma2/(D^2);
    y21= (yB+yA)/2 + (yA-yB)*(RB^2-R2^2)/(2*D^2) - 2*(xB-xA)*Sigma2/(D^2);   
    
    coeff_direct_tangente(1,:) = (y21 - y11)/(x21 - x11);
    
    % Compute second tangent (que l'on veut)
    x12= (xA+xB)/2 + (xB-xA)*(RA^2-R1^2)/(2*D^2) - 2*(yA-yB)*Sigma1/(D^2);
    y12= (yA+yB)/2 + (yB-yA)*(RA^2-R1^2)/(2*D^2) + 2*(xA-xB)*Sigma1/(D^2);
    x22= (xB+xA)/2 + (xA-xB)*(RB^2-R2^2)/(2*D^2) - 2*(yB-yA)*Sigma2/(D^2);
    y22= (yB+yA)/2 + (yA-yB)*(RB^2-R2^2)/(2*D^2) + 2*(xB-xA)*Sigma2/(D^2);   
    
    coeff_direct_tangente(2,:) = (y22 - y12)/(x22 - x12);
    
    %if (coeff_direct_tangente(1,:) > coeff_direct_tangente(2,:))
    if (abs(coeff_direct_tangente(1,:)) > abs(coeff_direct_tangente(2,:)))
        % Display tangent
%         plot (x11,y11,'og');
%         plot (x21,y21,'og');
%         line ([x11 x21],[y11 y21],'Color','g');
    
        Pt_tangente_haut(1,1) = x21;
        Pt_tangente_haut(1,2) = y21;
        
        Pt_tangente_bas(1,1) = x11;
        Pt_tangente_bas(1,2) = y11;
    else
        % Display tangent
%         plot (x12,y12,'or');
%         plot (x22,y22,'or');
%         line ([x12 x22],[y12 y22],'Color','r');
    
        Pt_tangente_haut(1,1) = x22;
        Pt_tangente_haut(1,2) = y22;
        
        Pt_tangente_bas(1,1) = x12;
        Pt_tangente_bas(1,2) = y12;
    end
    
    %% Recuperation des arcs cercle utile a l'affichage
    [row_h,col_h] = find(Pt_tangente_haut(1,2) < Coor_cercle_haut(:,2) & Coor_cercle_haut(:,2) < Pt_haut(1,2));

    Arc_haut(:,1) = Coor_cercle_haut(row_h(ceil(size(row_h,1)/2):end),1);
    Arc_haut(:,2) = Coor_cercle_haut(row_h(ceil(size(row_h,1)/2):end),2);

    [row_b,col_b] = find(Pt_bas(1,2) < Coor_cercle_bas(:,2) & Coor_cercle_bas(:,2) < Pt_tangente_bas(1,2));

    Arc_bas(:,1) = Coor_cercle_bas(row_b(ceil(size(row_b,1)/2):end),1);
    Arc_bas(:,2) = Coor_cercle_bas(row_b(ceil(size(row_b,1)/2):end),2);


    %% Affichage figure
    % figure(2)
    % axis square equal
    % grid on;
    % scatter(Arc_haut(:,1), Arc_haut(:,2))


    figure
    % Clear the axes.
    %cla

    % Fix the axis limits.
    % xlim([0 8.1])
    % ylim([-0.1 5.1])

    % Set the axis aspect ratio to 1:1.
    axis square equal
    grid on;

    %Cercle haut
    %viscircles(C_cercle_haut,R_cercle_haut,'Color',colors{1});
    %plot(Coor_cercle_haut(:,1), Coor_cercle_haut(:,2))
    %hold on
    plot (C_cercle_haut(1,1),C_cercle_haut(1,2),'xk');
    hold on
    %Cercle bas
    %viscircles(C_cercle_bas,R_cercle_bas,'Color',colors{2});
    plot (C_cercle_bas(1,1),C_cercle_bas(1,2),'xk');
    %plot(Coor_cercle_bas(:,1), Coor_cercle_bas(:,2))

    %Droite haut
    line ([Pt_haut(1,1) Pt_init(1,1)],[Pt_haut(1,2) Pt_init(1,2)],'Color','r');
    %plot(D_haut(:,1), D_haut(:,2))

    %Droite bas
    line ([Pt_bas(1,1) Pt_arrive(1,1)],[Pt_bas(1,2) Pt_arrive(1,2)],'Color','r');
    %plot(D_bas(:,1), D_bas(:,2))

    %Plot tangente
    %plot(D_milieu(:,1), D_milieu(:,2))

    %line (Pt_tangente_bas,Pt_tangente_haut,'Color','g');
    line ([Pt_tangente_bas(1,1) Pt_tangente_haut(1,1)],[Pt_tangente_bas(1,2) Pt_tangente_haut(1,2)],'Color','r');

    %Arc haut
    plot(Arc_haut(:,1), Arc_haut(:,2),'Color','r')

    %Arc bas
    plot(Arc_bas(:,1), Arc_bas(:,2),'Color','r')

    hold off

else
    disp ('No internal tangents');
end







