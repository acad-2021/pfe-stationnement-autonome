close all
%Definition des variables necessaire pour la tringulatino et les RFID
% %RFID 1
% X_RFID_1 = 2;
% Y_RFID_1 = 4;
% R_RFID_1 = 6;
% 
% %RFID 2
% X_RFID_2 = 2;
% Y_RFID_2 = 6;
% R_RFID_2 = 6;
% 
% %RFID 3
% X_RFID_3 = 4;
% Y_RFID_3 = 4;
% R_RFID_3 = 6;

%Ecartements entre RFID
L_ecarte_1_3 = 2;
H_ecarte_1_2 = 2;
R = 22;

% %RFID 1
%RFID 1
% X_RFID_1 = 15.674297680642132;
% Y_RFID_1 = 12.580992268807108;

% X_RFID_1 = 12;
% Y_RFID_1 = 15;

X_RFID_1 = 25;
Y_RFID_1 = 4;
R_RFID_1 = R;

%RFID 2
X_RFID_2 = X_RFID_1;
Y_RFID_2 = Y_RFID_1 + H_ecarte_1_2;
R_RFID_2 = R;

%RFID 3
X_RFID_3 = X_RFID_1 + L_ecarte_1_3;
Y_RFID_3 = Y_RFID_1;
R_RFID_3 = R;

%Offset simulation
x1 = X_RFID_1;
y1 = Y_RFID_1;

% X_voit_test = 27;
% Y_voit_test = 15;

%Calcul des distances
% r1 = sqrt( (X_voit_test - X_RFID_1)^2 + (Y_voit_test - Y_RFID_1)^2);
% r2 = sqrt( (X_voit_test - X_RFID_2)^2 + (Y_voit_test - Y_RFID_2)^2);
% r3 = sqrt( (X_voit_test - X_RFID_3)^2 + (Y_voit_test - Y_RFID_3)^2);
% 
% r1ca = (X_voit_test - X_RFID_1)^2 + (Y_voit_test - Y_RFID_1)^2;
% r2ca = (X_voit_test - X_RFID_2)^2 + (Y_voit_test - Y_RFID_2)^2;
% r3ca = (X_voit_test - X_RFID_3)^2 + (Y_voit_test - Y_RFID_3)^2;



%% Tracage des RFIDs avec leurs perimetres de detection

% figure
% colors = {'b','r','g'}; 
% set(gcf,'color', 'w')
% xlabel('X')
% ylabel('Y')
% % Create 5 random circles to display,
% X = [X_RFID_1 ; X_RFID_2 ; X_RFID_3];
% Y = [Y_RFID_1 ; Y_RFID_2 ; Y_RFID_3];
% 
% % X = rand(5,1);
% % Y = rand(5,1);
% 
% centers = [X Y];
% radii = [R_RFID_1 ; R_RFID_2 ; R_RFID_3];
% 
% % Clear the axes.
% cla
% 
% % Fix the axis limits.
% %xlim([-0.1 1.1])
% %ylim([-0.1 1.1])
% 
% % Set the axis aspect ratio to 1:1.
% axis square
% 
% % Set a title.
% title('RFIDs avec leurs perimetres de detection')
% 
% % Display the circles.
% for i = 1 : size(X,1)
%     viscircles(centers(i,:),radii(i,:),'Color',colors{i});
%     hold on
%     scatter(X(i,:)',Y(i,:)',50,colors{i},'filled');
% end
% 
% %scatter(X_voit_test,Y_voit_test,50,'m','filled');
