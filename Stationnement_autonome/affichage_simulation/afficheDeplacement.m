% le sang de la vanne : https://fr.mathworks.com/help/driving/ug/automated-parking-valet.html
% data = load('parkingLotCostmap.mat');% exemple de base


%close
%clear

%% Generation de la map
figure
parkMap = helperSLCreateCostmap();
plot(parkMap, 'Inflation', 'off');
legend off % pas utile dans notre cas
set(gcf,'color', 'w')
hold on
 
%Ajout des cercles de detection des RFIDs
colors = {'b','r','g'};
X = [X_RFID_1 ; X_RFID_2 ; X_RFID_3];
Y = [Y_RFID_1 ; Y_RFID_2 ; Y_RFID_3];

centers = [X Y];
radii = [R_RFID_1 ; R_RFID_2 ; R_RFID_3];

%Affichage des cercles de detection
for i = 1 : size(X,1)
    viscircles(centers(i,:),radii(i,:),'Color',colors{i});
    hold on
    scatter(X(i,:)',Y(i,:)',50,colors{i},'filled');
end

%%%Ajout de la trajectoire
% Set the axis aspect ratio to 1:1.
%axis square equal
grid on;

%Cercle haut
%viscircles(C_cercle_haut,R_cercle_haut,'Color',colors{1});
%plot(Coor_cercle_haut(:,1), Coor_cercle_haut(:,2))
%hold on
plot (C_cercle_haut(1,1),C_cercle_haut(1,2),'xk');
hold on
%Cercle bas
%viscircles(C_cercle_bas,R_cercle_bas,'Color',colors{2});
plot (C_cercle_bas(1,1),C_cercle_bas(1,2),'xk');
%plot(Coor_cercle_bas(:,1), Coor_cercle_bas(:,2))

%Droite haut
line ([Pt_haut(1,1) Pt_init(1,1)],[Pt_haut(1,2) Pt_init(1,2)],'Color','r');
%plot(D_haut(:,1), D_haut(:,2))

%Droite bas
line ([Pt_bas(1,1) Pt_arrivee(1,1)],[Pt_bas(1,2) Pt_arrivee(1,2)],'Color','r');
%plot(D_bas(:,1), D_bas(:,2))

%Plot tangente
%plot(D_milieu(:,1), D_milieu(:,2))

%line (Pt_tangente_bas,Pt_tangente_haut,'Color','g');
line ([Pt_tangente_bas(1,1) Pt_tangente_haut(1,1)],[Pt_tangente_bas(1,2) Pt_tangente_haut(1,2)],'Color','r');

%Arc haut
plot(Arc_haut(:,1), Arc_haut(:,2),'Color','r')

%Arc bas
plot(Arc_bas(:,1), Arc_bas(:,2),'Color','r')

%%%Ajout de la trajectoire reelle de la voiture
ligne_voiture=plot(pos_voiture(:,1),pos_voiture(:,2),'xk','Color','b','MarkerSize',1);

%plot(Pt_important_Trajectoire(:,1), Pt_important_Trajectoire(:,2),'Color','g');
% line ([15 Pt_init(1,1)],[4 Pt_init(1,2)],'Color','b');
% hold on
% line ([15 Pt_arrivee(1,1)],[4 Pt_arrivee(1,2)],'Color','g');


%% Generation de la voiture
% c'est � dire la forme qu'elle a, on prend ses param�tres de taille etc
% ...
carDims = parkMap.CollisionChecker.VehicleDimensions;
testcarDims = vehicleDimensions();

carPose = [x_initial,y_initial,theta_initial]; %[x,y,theta]

% carShape = move(templateShape,carPose(1),carPose(2),carPose(3));
carShape=helperVehiclePolyshape(carPose,carDims,45);

hold on

%Affichage de la voiture
previous = plot(carShape);

%pos_voiture=out.position_voiture.Data(:,1);
%pos_voiture = pos_voiture.Data(:,1);

iter_simu=length(pos_voiture);

%% Animation test
for i=1:iter_simu
    
    %On efface la representation de la voiture precedante
    delete(previous);
    %x=x+0.5;          %on deplace la voiture d'un petit peu
    
    %Nouvelle position de la voiture
    x = pos_voiture(i,1);
    y = pos_voiture(i,2);
    theta = orientation_voiture(i,1);
    
    carPose = [x,y,theta];
    carShape=helperVehiclePolyshape(carPose,carDims,0);
    
    %Affichage de la voiture
    previous=plot(carShape);
    %Pause pour eviter l'instantane 
    pause(0.000001);
    
end


