close
clear

%% Parametres initiaux
%Variables pour les RFID et la triangulation
systeme_RFID_et_triangulation_ctes;

%Parametres de la voiture
%Longueur de la voiture
l=1.5;
%Largeur de la voiture
largeur_voiture=2;
%Rayon de braquage maximum de la voiture (on doit effectuer des virages
%avec des rayons minimum de R_braquage_maximum
R_braquage_maximum = 2.5;
%Gain du correcteur PI pour l'asservissement en position
gain_PI_position = 1000;

%Position initiale
x_initial = 10;
y_initial = 15;
%y_initial = 4;
theta_initial=0; %A rajouter dans calcul_sigmoid !!!!!!!!!!!!

%Tolerance par rapport au point d'arrivee
Tolerance_pt_arrivee = 0.5;

%Calcul de l'itineraire
%calcul_trajectoire;

%Cercle en haut de la courbe
R_cercle_haut = 4;
X_C_cercle_haut = 12;

%Cercle en bas de la courbe
R_cercle_bas = 4;
X_C_cercle_bas = 22;

calcul_sigmoid;
%Pt_important_Trajectoire = [Pt_init ; 15 4 ; Pt_arrivee];
%angle_de_braquage = zeros(1,2);
%angle_de_braquage(1,1) = 90;
%angle_de_braquage(1,2) = 90;

% Pt_important_Trajectoire(4:end,:) = [];
% angle_de_braquage(:,3:end) = [];
%C_cercle_trajectoire(2,:) = [];

%% Appel de la simulation
%Lien pour la lancer la simulation :
%https://www.mathworks.com/help/simulink/ug/using-the-sim-command.html

simOut = sim('simulation_stationnement_simple_V2', ...
            'SimulationMode','normal', ...
            'AbsTol','1e-5',...
            'CaptureErrors', 'on',...
            'SaveState','on',...
            'SaveOutput','on','OutputSaveName','position_voiture',...
 'SaveFormat', 'Dataset');
pos_voiture_x = simOut.get('position_voiture_x');
pos_voiture_y = simOut.get('position_voiture_y');
orientation_voiture = simOut.get('orientation_voiture');

erreur_asser = simOut.get('erreur_asser');
erreur_asser = erreur_asser.Data(:,1);

index = simOut.get('index');
index = index.Data(:,1);

sens_braquage_out_sim = simOut.get('sens_braquage_out_sim');
sens_braquage_out_sim = sens_braquage_out_sim.Data(:,1);

angle_vect_out_sim = simOut.get('angle_vect_out_sim');
angle_vect_out_sim = angle_vect_out_sim.Data(:,1);

angle_vect_voit_sim = simOut.get('angle_arc_voit');
angle_vect_voit_sim = angle_vect_voit_sim.Data(:,1);

angle_vect_theo_sim = simOut.get('angle_arc_theo');
angle_vect_theo_sim = angle_vect_theo_sim.Data(:,1);

pos_voiture(:,1) = pos_voiture_x.Data(:,1);
pos_voiture(:,2) = pos_voiture_y.Data(:,1);
orientation_voiture = orientation_voiture.Data(:,1);

%Erreur a gerer si necessaire
error_sim = simOut.getSimulationMetadata.ExecutionInfo;

%%%%%%%%%%===========> Recuperer les erreurs de la simulation



%% Affichage debugogage
figure(2)
plot((1:length(index)), index);
hold on
plot((1:length(erreur_asser)), erreur_asser);
legend('index', 'erreur asserv')
title('Courbe debogage erreur_asser et index')
xlabel('num tick simulation')
ylabel('index et val erreur asser')

set(gcf,'color', 'w')
hold off

% figure(4)
% plot(1:1:4001,sens_braquage_out_sim_av_modulo)
% hold on
% plot(1:1:4001,sens_braquage_out_sim_modulo)
% 
% legend('sens braquage out sim av modulo', 'sens braquage out sim modulo')
% title('Courbe debogage trajectoire angle')
% xlabel('num tick simulation')
% ylabel('angle �')
% 
% set(gcf,'color', 'w')
% hold off

figure(5)
plot(1:1:4001,sens_braquage_out_sim)

legend('sens braquage out sim')
title('Courbe debogage trajectoire angle 2')
xlabel('num tick simulation')
ylabel('angle �')

set(gcf,'color', 'w')
hold off

figure(6)
plot(1:1:4001,angle_vect_out_sim)
hold on
plot(1:1:4001,angle_vect_theo_sim)
plot(1:1:4001,angle_vect_voit_sim)
plot(1:1:4001,orientation_voiture)

legend('angle vect out sim', 'angle vect theo sim', 'angle vect voit sim', 'orientation_voiture')
title('Courbe debogage trajectoire angle')
xlabel('num tick simulation')
ylabel('angle �')

set(gcf,'color', 'w')
hold off


%% Affichage de la simulation
afficheDeplacement;









