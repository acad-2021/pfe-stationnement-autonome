% clear
% close all
% 
% %Parametres de la voiture
% %Longueur de la voiture
% l=1.5;
% %Largeur de la voiture
% largeur_voiture=2;
% %Rayon de braquage maximum de la voiture (on doit effectuer des virages
% %avec des rayons minimum de R_braquage_maximum
% R_braquage_maximum = 2.5;
% 
% x_initial = 5;
% y_initial = 15;
% 
% X_RFID_1 = 25;
% Y_RFID_1 = 5;

%Affichage de la figure
verbose = 1;
%% Variables initiales
%Points initiaux
x_pt_dep = x_initial;
y_pt_dep = y_initial;

x_pt_arrivee = X_RFID_1;
y_pt_arrivee = Y_RFID_1;

% %Cercle en haut de la courbe
% R_cercle_haut = 4;
% X_C_cercle_haut = 12;
% 
% %Cercle en bas de la courbe
% R_cercle_bas = 4;
% X_C_cercle_bas = 22;

colors = {'b','r','g'};

%% Definition des points de depart et d'arrivee
%Point initiale de la voiture
%Pt_init(1,1) = 10;
Pt_init(1,1) = x_pt_dep;
%Pt_init(1,2) = 5;
Pt_init(1,2) = y_pt_dep;

%Point arrive de la voiture
% Pt_arrive(1,1) = 0;
Pt_arrivee(1,1) = x_pt_arrivee;
% Pt_arrive(1,2) = 0;
Pt_arrivee(1,2) = y_pt_arrivee;

%% Definition des 2 cercles principaux
% Intervalle entre 2 points du cercle
th = 0:pi/100:2*pi;

%Cercle en haut de la courbe
%R_cercle_haut = 2;
C_cercle_haut = [X_C_cercle_haut Pt_init(1,2)-R_cercle_haut];

Coor_cercle_haut =  zeros(size(th,2),2);
Coor_cercle_haut(:,1) = R_cercle_haut * cos(th) + C_cercle_haut(:,1);
Coor_cercle_haut(:,2) = R_cercle_haut * sin(th) + C_cercle_haut(:,2);

%Cercle en bas de la courbe
%R_cercle_bas = 1;
C_cercle_bas = [X_C_cercle_bas Pt_arrivee(1,2)+R_cercle_bas];

Coor_cercle_bas =  zeros(size(th,2),2);
Coor_cercle_bas(:,1) = R_cercle_bas * cos(th) + C_cercle_bas(:,1);
Coor_cercle_bas(:,2) = R_cercle_bas * sin(th) + C_cercle_bas(:,2);

%% Definition des 2 points tangents aux droites
%Point le plus haut de la courbe
Pt_haut(1,1) = C_cercle_haut(:,1);
Pt_haut(1,2) = Pt_init(1,2);

%Point le plus bas de la courbe
Pt_bas(1,1) = C_cercle_bas(:,1);
Pt_bas(1,2) = Pt_arrivee(1,2);

%% Calcul des tangentes interieures
%Site de reference https://lucidar.me/fr/mathematics/tangent-line-segments-to-circle/
% Circle A
xA=C_cercle_bas(:,1);
yA=C_cercle_bas(:,2);
RA=R_cercle_bas;

% Circle B
xB=C_cercle_haut(:,1);
yB=C_cercle_haut(:,2);
RB=R_cercle_haut;

% Draw circles
% a=[0:pi/16:2*pi];
% Xa=xA+RA*cos(a);
% Ya=yA+RA*sin(a);
% plot (Xa,Ya,'k');
% hold on;
% axis square equal;
% grid on;
% plot (xA,yA,'xk');
% 
% 
% Xb=xB+RB*cos(a);
% Yb=yB+RB*sin(a);
% plot (Xb,Yb,'k');
% plot (xB,yB,'xk');

% Compute distance between circle centers
D=sqrt ( (xB-xA)^2 + (yB-yA)^2 );

coeff_direct_tangente = zeros(2,1);

if (D^2 - (RA+RB)^2>=0)
    
    % Compute the length of the tangents
    L=sqrt(D^2 - (RA+RB)^2);

    % Compute the parameters
    R1=sqrt(L^2+RB^2);    
    Sigma1= (1/4) * sqrt ( ( D+RA+R1 )*( D+RA-R1 )*( D-RA+R1 )*( -D+RA+R1 )  );
    R2=sqrt(L^2+RA^2);
    Sigma2= (1/4) * sqrt ( ( D+RB+R2 )*( D+RB-R2 )*( D-RB+R2 )*( -D+RB+R2 )  );
    
    
    % Compute the first tangent
    x11= (xA+xB)/2 + (xB-xA)*(RA^2-R1^2)/(2*D^2) + 2*(yA-yB)*Sigma1/(D^2);
    y11= (yA+yB)/2 + (yB-yA)*(RA^2-R1^2)/(2*D^2) - 2*(xA-xB)*Sigma1/(D^2);
    x21= (xB+xA)/2 + (xA-xB)*(RB^2-R2^2)/(2*D^2) + 2*(yB-yA)*Sigma2/(D^2);
    y21= (yB+yA)/2 + (yA-yB)*(RB^2-R2^2)/(2*D^2) - 2*(xB-xA)*Sigma2/(D^2);   
    
    coeff_direct_tangente(1,:) = (y21 - y11)/(x21 - x11);
    
    % Compute second tangent (que l'on veut)
    x12= (xA+xB)/2 + (xB-xA)*(RA^2-R1^2)/(2*D^2) - 2*(yA-yB)*Sigma1/(D^2);
    y12= (yA+yB)/2 + (yB-yA)*(RA^2-R1^2)/(2*D^2) + 2*(xA-xB)*Sigma1/(D^2);
    x22= (xB+xA)/2 + (xA-xB)*(RB^2-R2^2)/(2*D^2) - 2*(yB-yA)*Sigma2/(D^2);
    y22= (yB+yA)/2 + (yA-yB)*(RB^2-R2^2)/(2*D^2) + 2*(xB-xA)*Sigma2/(D^2);   
    
    coeff_direct_tangente(2,:) = (y22 - y12)/(x22 - x12);
    
    %if (coeff_direct_tangente(1,:) > coeff_direct_tangente(2,:))
    if (abs(coeff_direct_tangente(1,:)) > abs(coeff_direct_tangente(2,:)))
        % Display tangent
%         plot (x11,y11,'og');
%         plot (x21,y21,'og');
%         line ([x11 x21],[y11 y21],'Color','g');
    
        Pt_tangente_haut(1,1) = x21;
        Pt_tangente_haut(1,2) = y21;
        
        Pt_tangente_bas(1,1) = x11;
        Pt_tangente_bas(1,2) = y11;
    else
        % Display tangent
%         plot (x12,y12,'or');
%         plot (x22,y22,'or');
%         line ([x12 x22],[y12 y22],'Color','r');
    
        Pt_tangente_haut(1,1) = x22;
        Pt_tangente_haut(1,2) = y22;
        
        Pt_tangente_bas(1,1) = x12;
        Pt_tangente_bas(1,2) = y12;
    end
    
    %% Recuperation des arcs cercle utile a l'affichage
    if(Pt_init(1,1) > Pt_arrivee(1,1))
        [row_h,col_h] = find(Pt_tangente_haut(1,2) < Coor_cercle_haut(:,2) & Coor_cercle_haut(:,2) < Pt_haut(1,2));

        Arc_haut(:,1) = Coor_cercle_haut(row_h(ceil(size(row_h,1)/2):end),1);
        Arc_haut(:,2) = Coor_cercle_haut(row_h(ceil(size(row_h,1)/2):end),2);

        [row_b,col_b] = find(Pt_bas(1,2) < Coor_cercle_bas(:,2) & Coor_cercle_bas(:,2) < Pt_tangente_bas(1,2));

        Arc_bas(:,1) = Coor_cercle_bas(row_b(ceil(size(row_b,1)/2):end),1);
        Arc_bas(:,2) = Coor_cercle_bas(row_b(ceil(size(row_b,1)/2):end),2);
    else
        [row_h,col_h] = find(Pt_tangente_haut(1,2) < Coor_cercle_haut(:,2) & Coor_cercle_haut(:,2) < Pt_haut(1,2));
        
        Arc_haut=zeros(size(row_h(1:ceil(size(row_h,1)/2)),1),2);
        
        Arc_haut(:,1) = Coor_cercle_haut(row_h(1:ceil(size(row_h,1)/2)),1);
        Arc_haut(:,2) = Coor_cercle_haut(row_h(1:ceil(size(row_h,1)/2)),2);

        [row_b,col_b] = find(Pt_bas(1,2) < Coor_cercle_bas(:,2) & Coor_cercle_bas(:,2) < Pt_tangente_bas(1,2));
        
        Arc_bas=zeros(size(row_b(1:ceil(size(row_b,1)/2)),1),2);
        Arc_bas(:,1) = Coor_cercle_bas(row_b(1:ceil(size(row_b,1)/2)),1);
        Arc_bas(:,2) = Coor_cercle_bas(row_b(1:ceil(size(row_b,1)/2)),2);
    end

    %% Calcul des angles de braquages
    %l=1.5;
    %largeur_voiture=2;
    %R = 2.5
    
    %Droite en haut
    angle_de_braquage(1,1) = 90;
    
    %Cercle haut
    %L'arc de cercle part vers la droite
    if(Pt_haut(1,2) > Pt_tangente_haut(1,2))
        angle_de_braquage(1,2) = atand( (-R_cercle_haut/l) + (largeur_voiture/(2*l)) ) + 180;
    %L'arc de cercle part vers la gauche
    else
        angle_de_braquage(1,2) = atand( (R_cercle_haut/l) - (largeur_voiture/(2*l)) );
    end
    
    %Tangente cercle
    angle_de_braquage(1,3) = 90;
    
    %Cercler bas
    %L'arc de cercle part vers la droite
    if(Pt_tangente_bas(1,2) > Pt_bas(1,2))
        angle_de_braquage(1,4) = atand( (R_cercle_bas/l) - (largeur_voiture/(2*l)) );
        %angle_de_braquage(1,4) = atand( (-R_cercle_bas/l) + (largeur_voiture/(2*l)) ) + 180;
    %L'arc de cercle part vers la gauche
    else
        %angle_de_braquage(1,4) = atand( (R_cercle_bas/l) - (largeur_voiture/(2*l)) );
        angle_de_braquage(1,4) = atand( (-R_cercle_bas/l) + (largeur_voiture/(2*l)) ) + 180;
    end
    
    %Droite en bas
    angle_de_braquage(1,5) = 90;
    
    
%     if(direction>90)  
%         R=-(tand(180-direction)*l+largeur_voiture/2)
%     else
%         R=tand(direction)*l+largeur_voiture/2
%     end
    
%     l=1.5;
%     largeur_voiture=2;
%     R = 2.5
%     45�
    internal_tangent_possible = true;
   
    
    %% Affichage figure
    if(verbose==1)
        % figure(2)
        % axis square equal
        % grid on;
        % scatter(Arc_haut(:,1), Arc_haut(:,2))


        figure
        % Clear the axes.
        %cla

        % Fix the axis limits.
        % xlim([0 8.1])
        % ylim([-0.1 5.1])

        % Set the axis aspect ratio to 1:1.
        axis square equal
        grid on;

        %Cercle haut
        %viscircles(C_cercle_haut,R_cercle_haut,'Color',colors{1});
        plot(Coor_cercle_haut(:,1), Coor_cercle_haut(:,2))
        hold on
        plot (C_cercle_haut(1,1),C_cercle_haut(1,2),'xk');
        hold on
        %Cercle bas
        %viscircles(C_cercle_bas,R_cercle_bas,'Color',colors{2});
        plot (C_cercle_bas(1,1),C_cercle_bas(1,2),'xk');
        plot(Coor_cercle_bas(:,1), Coor_cercle_bas(:,2))

        %Droite haut
        line ([Pt_haut(1,1) Pt_init(1,1)],[Pt_haut(1,2) Pt_init(1,2)],'Color','r');
        %plot(D_haut(:,1), D_haut(:,2))

        %Droite bas
        line ([Pt_bas(1,1) Pt_arrivee(1,1)],[Pt_bas(1,2) Pt_arrivee(1,2)],'Color','r');
        %plot(D_bas(:,1), D_bas(:,2))

        %Plot tangente
        %plot(D_milieu(:,1), D_milieu(:,2))

        %line (Pt_tangente_bas,Pt_tangente_haut,'Color','g');
        line ([Pt_tangente_bas(1,1) Pt_tangente_haut(1,1)],[Pt_tangente_bas(1,2) Pt_tangente_haut(1,2)],'Color','r');

        %Arc haut
        plot(Arc_haut(:,1), Arc_haut(:,2),'Color','r')

        %Arc bas
        plot(Arc_bas(:,1), Arc_bas(:,2),'Color','r')

        %hold off
    end
    
    C_cercle_trajectoire = [C_cercle_haut ; C_cercle_bas];
    R_cercle_trajectoire = [R_cercle_haut ; R_cercle_bas];
    Pt_important_Trajectoire = [Pt_init ; Pt_haut ; Pt_tangente_haut ; Pt_tangente_bas ; Pt_bas ; Pt_arrivee];
    
    %Nb_pts_trajectoire = size(Pt_important_Trajectoire,1);

%%Test calcul_erreur_sigmoid
% numero_cercle =1;
% index_checkpoint_precedent = 1;
% index_pred = index_checkpoint_precedent;
% 
% % x_voit = 15.5428;
% % y_voit = 12.8570;
% % x_voit = Pt_important_Trajectoire(index_pred+1,1);
% % y_voit = Pt_important_Trajectoire(index_pred+1,2);
% 
% x_voit = 11;
% y_voit = 14;
% 
% %Au milieu checkpoint
% % x_voit = 15.5428;
% % y_voit = 12.8570;
% % 
% % a = C_cercle_haut(1,1);
% % b = C_cercle_haut(1,2);
% % R = R_cercle_haut;
% % x = x_voit
% % syms y %x a b R
% % eqn = (y-b)^2 + (x-a)^2 == R^2;
% % solsym = solve(eqn, y)
% % y_voit = double(solsym(2))
% 
% 
% % Xa=xA+RA*cos(a);
% % Ya=yA+RA*sin(a);
% % x_voit = C_cercle_haut(1,1); +R_cercle_haut .* cos(pi/4);
% % y_voit = C_cercle_haut(1,2); +R_cercle_haut .* sin(pi/4);
% 
% 
% 
% %avant checkpoint
% % x_voit = 14.55;
% % y_voit = 14.08;
% 
% %apres checkpoint
% % x_voit = 15.95;
% % y_voit = 11.63;
% 
% 
% % x_bas = 1;
% % y_bas = 1;
% 
% 
%     distance_arrivee = sqrt((X_RFID_1 - x_voit)^2 + (Y_RFID_1 - y_voit)^2);
% 
%     if(distance_arrivee <= Tolerance_pt_arrivee)
%         erreur = 0;
%         norm_vect = 0;
%         angle_vect = 0;
%     else
% 
%         %pt_trajectoire = zeros(Subdivision,2);
%         if(index_checkpoint_precedent == size(Pt_important_Trajectoire,1))
%             col = index_checkpoint_precedent - 1;
%         else
%             col = index_checkpoint_precedent;
%         end
% 
%         
% 
%         %Ligne droite
%         if(angle_de_braquage(1, col) == 90)
%             %Calcul du vecteur de la droite theorique
%             %et la position voiture
%             vec_d_theo = zeros(1,2);
%             vec_d_theo(1,1) = Pt_important_Trajectoire(index_pred,1) - Pt_important_Trajectoire(index_pred+1,1);
%             vec_d_theo(1,2) = Pt_important_Trajectoire(index_pred,2) - Pt_important_Trajectoire(index_pred+1,2);
% 
%             %Norme du vercteur
%             norm_vec_d_theo = norm(vec_d_theo,2);
% 
%             %Calcul du vecteur entre le point de depart de la droite theorique
%             %et la position voiture
%             vec_d_voit = zeros(1,2);
%             vec_d_voit(1,1) = x_voit - Pt_important_Trajectoire(index_pred+1,1);
%             vec_d_voit(1,2) = y_voit - Pt_important_Trajectoire(index_pred+1,2);
% 
%             %Norme du vercteur
%             norm_vec_d_voit = norm(vec_d_voit,2);
% 
%             %Calcul de la distance minimale
%             distance_projection = (vec_d_voit * vec_d_theo')/norm_vec_d_theo;
% 
%             distance_min = sqrt(norm_vec_d_voit^2 - distance_projection^2);
% 
%             %distance_min = norm_vec_d_voit * nv_y_voit/norm_vec_d_voit;
% 
%             %angle_vect = asin(nv_y_voit/norm_vec_d_voit);
%             %Si l'angle est positif on est a droite du vecteur
%             %Si l'angle est negatif on est a gauche du vecteur
%             angle_vect = det([vec_d_voit ; vec_d_theo]');
%             
%             %Si angle_vect = 0 => les vecteurs sont colineaires ont des
%             %donc sur la droite
%             if(angle_vect > 0)
%                 sens_braquage = 1;
%             else
%                 sens_braquage = -1;
%             end
% 
%             if(norm_vec_d_voit >= norm_vec_d_theo)
%                 index_out = index_pred + 1;
%             else
%                 index_out = index_pred;
%             end
%             %norm_vect = norm_vec_d_voit;
%             err = distance_min;
% 
%         %Courbe (arc de cercle)
%         else
%             numero_cercle = nnz(angle_de_braquage(1,1:col)~=90);
% 
%             %Calcul du vecteur entre le centre du cercle et le point final de
%             %l'arc (checkpoint de fin de l'arc
%             vec_arc_theo = zeros(1,2);
%             
%             %Vec pour affichage (non replace dans la base)
%             vec_arc_theo_affi(1,1) = Pt_important_Trajectoire(index_pred+1,1) - C_cercle_trajectoire(numero_cercle,1);
%             vec_arc_theo_affi(1,2) = Pt_important_Trajectoire(index_pred+1,2) - C_cercle_trajectoire(numero_cercle,2);
%             
%             
%             
%             %nv_x_arc_theo = Pt_important_Trajectoire(index_pred+1,1) - C_cercle_trajectoire(numero_cercle,1)
%             vec_arc_theo(1,1) = (Pt_important_Trajectoire(index_pred+1,1) - C_cercle_trajectoire(numero_cercle,1)) %- C_cercle_trajectoire(numero_cercle,1);
%             vec_arc_theo(1,2) = (Pt_important_Trajectoire(index_pred+1,2) - C_cercle_trajectoire(numero_cercle,2)) %- C_cercle_trajectoire(numero_cercle,2);
%           
%             
%             %Norme du vercteur
%             norm_vec_arc_theo = norm(vec_arc_theo,2);
% 
%             angle_arc_theo = asin(Pt_important_Trajectoire(index_pred+1,2)/norm_vec_arc_theo);
%             %angle_arc_theo = asin((Pt_important_Trajectoire(index_pred+1,2) - C_cercle_trajectoire(numero_cercle,2))/norm_vec_arc_theo);
%             
%             %Calcul du vecteur entre le centre du cercle theorique
%             %et la position voiture
%             vec_arc_voit = zeros(1,2);
%             
%             %Vec pour affichage (non replace dans la base)
%             vec_arc_voit_affi(1,1) = x_voit - C_cercle_trajectoire(numero_cercle,1)
%             vec_arc_voit_affi(1,2) = y_voit - C_cercle_trajectoire(numero_cercle,2)
%             
%             vec_arc_voit(1,1) = (x_voit - C_cercle_trajectoire(numero_cercle,1)) %- C_cercle_trajectoire(numero_cercle,1)
%             vec_arc_voit(1,2) = (y_voit - C_cercle_trajectoire(numero_cercle,2)) %- C_cercle_trajectoire(numero_cercle,2)
% 
%             %Norme du vecteur
%             norm_vec_arc_voit = norm(vec_arc_voit,2);
% 
%             %Calcul de l'angle du vecteur par rapport a la base
%             angle_arc_voit = asin(y_voit/norm_vec_arc_voit);
%             %angle_arc_voit = asin((y_voit - C_cercle_trajectoire(numero_cercle,2))/norm_vec_arc_voit);
% 
%             %Calcul de la distance minimale
%             %distance_min = norm_vec_arc_voit - R_cercle_trajectoire(numero_cercle,1);
%             %=> faudrait en fait soustraire pour chaque variable et refaire
%             %la norm non ?
%             
%             
%             %Si on fait �a on va avoir des distances negatives si la voit
%             %et plus proche du centre du cercle que la courbe possible ?
%             distance_min = norm_vec_arc_voit - R_cercle_trajectoire(numero_cercle,1);
%             %distance_min = norm_vec_arc_voit - norm_vec_arc_theo;
%             
% 
%             %On tourne vers la gauche
%             if(angle_de_braquage > 90)
%                 angle_vect = angle_arc_voit - angle_arc_theo;
%             %On tourne vers la droite
%             else
%                 angle_vect = angle_arc_voit - angle_arc_theo;
%                 %angle_vect = angle_arc_theo - angle_arc_voit;
%             end
% 
% 
%             norm_vect = norm_vec_arc_voit;
%             err = distance_min;
%         end
% 
%         erreur = err;
%     end
%     
% %    plot (x_voit,y_voit,'xk', 'Color', 'r');
% %   plot(Pt_important_Trajectoire(:,1), Pt_important_Trajectoire(:,2),'xk','Color', 'b');
%     
%     %Vec non replace dans la base
% %     line ([C_cercle_trajectoire(numero_cercle,1), vec_arc_theo_affi(1,1)+C_cercle_trajectoire(numero_cercle,1)],[C_cercle_trajectoire(numero_cercle,2) vec_arc_theo_affi(1,2)+C_cercle_trajectoire(numero_cercle,2)],'Color','g');
% %     line ([C_cercle_trajectoire(numero_cercle,1), vec_arc_voit_affi(1,1)+C_cercle_trajectoire(numero_cercle,1)],[C_cercle_trajectoire(numero_cercle,2) vec_arc_voit_affi(1,2)+C_cercle_trajectoire(numero_cercle,2)],'Color','b');
%     %Distance min / erreur
%     x_debut_erreur = C_cercle_trajectoire(numero_cercle,1)+R_cercle_trajectoire(numero_cercle,1);
%     x_fin_erreur = C_cercle_trajectoire(numero_cercle,1)+R_cercle_trajectoire(numero_cercle,1) + distance_min
%     
%     y_debut_erreur = C_cercle_trajectoire(numero_cercle,2)+R_cercle_trajectoire(numero_cercle,1);
%     y_fin_erreur = C_cercle_trajectoire(numero_cercle,2)+R_cercle_trajectoire(numero_cercle,1) + distance_min
%     
%     %line ([x_debut_erreur,  x_fin_erreur],[y_debut_erreur, y_fin_erreur],'Color','b');
%     
%     
% %     line ([0, vec_arc_theo(1,1)+C_cercle_trajectoire(numero_cercle,1)],[0 vec_arc_theo(1,2)+C_cercle_trajectoire(numero_cercle,2)],'Color','g');
% %     line ([0, vec_arc_voit(1,1)+C_cercle_trajectoire(numero_cercle,1)],[0 vec_arc_voit(1,2)+C_cercle_trajectoire(numero_cercle,2)],'Color','b');
% 
%     

else
    internal_tangent_possible = false;
    disp ('No internal tangents');
end




