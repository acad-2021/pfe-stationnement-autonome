clear
close all

%Parametres de la voiture
%Longueur de la voiture
l=1.5;
%Largeur de la voiture
largeur_voiture=2;
%Rayon de braquage maximum de la voiture (on doit effectuer des virages
%avec des rayons minimum de R_braquage_maximum
R_braquage_maximum = 2.5;

x_initial = 5;
y_initial = 15;

X_RFID_1 = 25;
Y_RFID_1 = 5;

%Zone d'exclusion
Pt_rectangle_exclusion(1,1) = x_initial;
Pt_rectangle_exclusion(1,2) = 5;

Pt_rectangle_exclusion(2,1) = 14;
Pt_rectangle_exclusion(2,2) = 14;

%Affichage de la figure
verbose = 1;

%% Variables initiales
%Points initiaux
x_pt_dep = x_initial;
y_pt_dep = y_initial;

x_pt_arrivee = X_RFID_1;
y_pt_arrivee = Y_RFID_1;

%Cercle en haut de la courbe
%R_cercle_haut = 4;
%X_C_cercle_haut = 12;
%X_C_cercle_haut = x_initial;

%Cercle en bas de la courbe
%R_cercle_bas = 4;
X_C_cercle_bas = 22;
%X_C_cercle_bas = x_pt_arrivee;

%calcul_sigmoid
%%% PENSER A GERER LE CAS R < 2.5
%%% PENSER A GERER LE CAS DE L'AUTRE COTE ET EN MIROIR

%On place le x du cercle en dehors de la zone (juste a cote ou a l'angle)
%pour avoir de la marge de manoeuvre et on met le rayon min possible de braquage
%X_C_cercle_haut = Pt_rectangle_exclusion(2,1) + 1;
X_C_cercle_haut = Pt_rectangle_exclusion(2,1) - R_braquage_maximum/2;
%Ce systeme pour gagner du rayon risque de poser probleme si on si on a une
%zone d'exclusion tres proche du y du point de depart

%On fait un truc pour le y ou c'est bon vu que l'on gere le y du cercle
%avec la ligne droite avec le pt de depart ?





%Si au cas d'initialisation est deja bloquer on va jamais pouvoir avoir de
%tangente interieur si on continue a augmenter le x du cercle du haut
%Il faudrait decaler le cercle du bas en verifiant qu'il ne depasse pas le
%point d'arrivee.
%Si on fait ca on initialise comment le x du cercle du bas on peut pas le
%mettre sur l'angle de la zone d'exclusion vu quelle peut etre plus loin
%que le point d'arrivee => on peut le placer au point d'arrivee mais si on
%fait ca on saura pas comment avoir un bon compris entre la position du x
%et le rayon du cercle (pour avoir un bon rayon et une droit apres l'arc)

cas_init_possible = false;
R_braquage = R_braquage_maximum;
R_cercle_bas = R_braquage;
R_cercle_haut = R_braquage;

while cas_init_possible == false
    calcul_sigmoid;
    close
    if(internal_tangent_possible == true)
        cas_init_possible = true
    else
        %A GERER AVEC LES REFLEXIONS DU DESSUS
        %X_C_cercle_haut = X_C_cercle_haut + 1;
    end
end


existence_tangent = true;
% R_braquage = R_braquage_maximum;
% R_cercle_bas = R_braquage;
% R_cercle_haut = R_braquage;

while existence_tangent == true
    calcul_sigmoid;
    rectangle('Position',[Pt_rectangle_exclusion(1,1) Pt_rectangle_exclusion(1,2)...
    Pt_rectangle_exclusion(2,1)-Pt_rectangle_exclusion(1,1) Pt_rectangle_exclusion(2,2)-Pt_rectangle_exclusion(1,2)],'EdgeColor', 'b');
    close
    if(internal_tangent_possible == true)
        R_braquage = R_braquage + 1;
        R_cercle_bas = R_braquage;
        R_cercle_haut = R_braquage;
        
        X_C_cercle_haut = Pt_rectangle_exclusion(2,1) - R_cercle_haut/2;
    else
        existence_tangent = false;
    end
end

R_cercle_bas = R_braquage - 1;
R_cercle_haut = R_braquage - 1;

X_C_cercle_haut = Pt_rectangle_exclusion(2,1) - R_cercle_haut/2;

calcul_sigmoid;
rectangle('Position',[Pt_rectangle_exclusion(1,1) Pt_rectangle_exclusion(1,2)...
    Pt_rectangle_exclusion(2,1)-Pt_rectangle_exclusion(1,1) Pt_rectangle_exclusion(2,2)-Pt_rectangle_exclusion(1,2)],'EdgeColor', 'b');



