![](Images/README/Logo_PFE.png)

# Projet de Fin d'Etude

Stationnement Autonome par détection RFID est un projet de fin d'étude réalisé à l'**INSA Centre Val de Loire**. Ce sujet de projet a été proposé par les élèves aux professeurs. 

Extrêmement pluridisciplinaire, il a été réalisé dans le cadre de l'option **ACAD** (ACquisition, Analyse et Décision).

Ce projet de fin d'étude a reçu la note très honorable de 18/20 avec les félicitations du jury.

## Introduction 

Les voitures autonomes commencent à prendre en importance dans le secteur automobile. Tous les grands constructeurs se livrent une course pour atteindre l'autonomie totale. 

La fonction de "stationnement" est un élément primordial pour atteindre celle-ci. De nos jours, des méthodes et technologies ont déjà été implémentées dans les véhicules haut de gamme. Pour atteindre un public plus large, il faut trouver de nouvelles solutions plus accessible.

La technologie RFID semble être une bonne solution avec un coût plus faible. Elle est idéale pour les zones restreintes comme des parkings ou des centres-villes.

Le concept de notre projet est donc d'utiliser des tags RFID pour guider une voiture sur sa futur place de stationnement et le tout de manière autonome, sans interactions du conducteur.

Pour se faire, nous avons réalisé une simulation numérique complète.

## Aperçus

> Animation finale de la simulation :![](Videos/Simulation_2_x4.gif)

> Schéma de modélisation de la voiture :
>
> ![](Images/README/modele_voiture.png)

## Pré-requis

Le projet a été développé sur **Matlab 2019** avec le moteur de simulation **Simulink**. 

Il faut donc avoir cette version (ou supérieure) pour exécuter le projet.

## Démarrage

1. Télécharger le dépôt sur son ordinateur

2. Ouvrir avec Matlab le fichier projet : **Asservissement_voiture.prj**

   Celui-ci se trouve à la racine du dossier **Stationnement_autonome**

3. Lancer le script **main.m**

4. La simulation se lance. Elle peut prendre un peu de temps là première fois. Quand la simulation est terminée, un affichage s'ouvre est réalise une animation de la simulation.

5. Les paramètres de l'emplacement de départ de la voiture et de la zone d'arrivée peuvent être modifié dans le script **main.m**

   Si les variables ont été initialisé une première fois. **simulation_stationnement_simple_V2.slx** peut être exécuté pour avoir uniquement la simulation sans l'affichage.

## Versions

25/01/2020 : Corrections Rapport / Présentation / Poster

22/01/2020 : Ajout du README

06/01/2021 : Asservissement fonctionnel 

12/12/2020 : Amélioration de la simulation (V2)

11/12/2020 : 1er test d'une simulation complète avec asservissement

09/11/2020 : Première simulation (V1)

28/10/2020 : Début du contrôle de version et ajout des premières modélisations de la voiture et de l'environnement.

16/10/2020 : Validation du sujet

06/10/2020 : Soumission du projet au responsable PFE

## To Do

- [x] Faire le README
- [x] Finir de corriger l'asservissement
- [x] Ranger le dépôt
- [x] Passer la soutenance 

## Auteurs

- :mortar_board: [Léo HUNOUT](https://gitlab.com/hunoutl) :floppy_disk:
- :trophy: [Floriflo](https://gitlab.com/Floriflo1) :computer:

**Comment citer ?**

Vie,F & Hunout,L (2021) PFE Stationnement autonome par détection RFID [INSA Centre Val de Loire] , https://gitlab.com/acad-2021/pfe-stationnement-autonome